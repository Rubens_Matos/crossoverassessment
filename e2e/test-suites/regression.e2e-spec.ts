import {LoginPage} from '../page-objects/pages/login/login.po';
import {WriteEmailPage} from '../page-objects/pages/login/writeEmail.po';
import {LoginPageHelper} from '../page-objects/pages/login/login-page.helper';
import {browser, ExpectedConditions} from 'protractor';

describe('Gmail suite', () => {
    let loginPageHelper: LoginPageHelper;
    let WriteEmail = new WriteEmailPage();
    let Login = new LoginPage();

    beforeEach(() => {
        loginPageHelper = new LoginPageHelper();
        browser.ignoreSynchronization = true;
    });

    it('Login email', async () => {
        await loginPageHelper.goToPage();
        await Login.username.sendKeys(browser.params.username);
        await Login.btnNextLogin.click();
        await browser.wait(ExpectedConditions.visibilityOf(Login.password));
        await Login.password.sendKeys(browser.params.password);
        await Login.passwordNextButton.click();
    });
    it('Write email', async () => {
        await WriteEmail.btnWrite.click();
        await WriteEmail.emailTO.clear();
        await WriteEmail.emailTO.sendKeys(browser.params.emailTO);
        await WriteEmail.writeTextEmail.sendKeys("TESTE");
        await WriteEmail.btnSend.click();
    });
});

