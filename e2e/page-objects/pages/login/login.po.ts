import { element, ElementFinder, By } from 'protractor';

export class LoginPage {
    password: ElementFinder = element(By.name('password'));
    username: ElementFinder = element(By.id('identifierId'));
    btnNextLogin: ElementFinder = element(By.id('identifierNext'));
    passwordNextButton: ElementFinder = element(By.id('passwordNext'));
}
