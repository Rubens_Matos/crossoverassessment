import { element, ElementFinder, By } from 'protractor';

export class WriteEmailPage {
    btnWrite: ElementFinder = element(By.buttonText('Escrever'));
    emailTO: ElementFinder = element(By.name("to"));
    writeTextEmail: ElementFinder = element(By.xpath('//*[@id=":9n"]'));
    btnSend: ElementFinder = element(By.xpath('//*[@role="button" and text()="Send"]'));
}
