"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const login_po_1 = require("../page-objects/pages/login/login.po");
const writeEmail_po_1 = require("../page-objects/pages/login/writeEmail.po");
const login_page_helper_1 = require("../page-objects/pages/login/login-page.helper");
const protractor_1 = require("protractor");
describe('Gmail suite', () => {
    let loginPageHelper;
    let WriteEmail = new writeEmail_po_1.WriteEmailPage();
    beforeEach(() => {
        loginPageHelper = new login_page_helper_1.LoginPageHelper();
    });
    it('Login email', () => __awaiter(this, void 0, void 0, function* () {
        yield loginPageHelper.goToPage();
        yield login_po_1.LoginPage.username.sendKeys(protractor_1.browser.params.username);
        yield protractor_1.element(protractor_1.By.id('identifierNext')).click();
        yield protractor_1.browser.wait(protractor_1.ExpectedConditions.visibilityOf(login_po_1.LoginPage.password));
        yield login_po_1.LoginPage.password.sendKeys(protractor_1.browser.params.password);
        yield login_po_1.LoginPage.passwordNextButton.click();
    }));
    it('Write email', () => __awaiter(this, void 0, void 0, function* () {
        yield WriteEmail.btnWrite.click();
        yield WriteEmail.emailTO.clear();
        yield WriteEmail.emailTO.sendKeys(protractor_1.browser.params.emailTO);
        yield WriteEmail.writeTextEmail.sendKeys("TESTE");
        yield WriteEmail.btnSend.click();
    }));
});
//# sourceMappingURL=regression.e2e-spec.js.map