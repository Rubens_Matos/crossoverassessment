"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class WriteEmailPage {
    constructor() {
        this.btnWrite = protractor_1.element(protractor_1.By.id("//*[@id=':ke']/div/div"));
        this.emailTO = protractor_1.element(protractor_1.By.name("to"));
        this.writeTextEmail = protractor_1.element(protractor_1.By.xpath('//*[@id=":9n"]'));
        this.btnSend = protractor_1.element(protractor_1.By.xpath('//*[@role="button" and text()="Send"]'));
    }
}
exports.WriteEmailPage = WriteEmailPage;
//# sourceMappingURL=writeEmail.po.js.map