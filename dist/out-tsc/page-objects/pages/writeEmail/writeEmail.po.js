"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class WriteEmailPage {
    static get btnWrite() {
        return protractor_1.element.all(protractor_1.By.xpath("//*[@id=':3b']/div/div"));
    }
    static get emailTO() {
        return protractor_1.element(protractor_1.By.id(":qd"));
    }
    static get writeTextEmail() {
        return protractor_1.element(protractor_1.By.xpath('//*[@id=":9n"]'));
    }
    static get btnSend() {
        return protractor_1.element(protractor_1.By.xpath('//*[@role="button" and text()="Send"]'));
    }
}
exports.WriteEmailPage = WriteEmailPage;
//# sourceMappingURL=writeEmail.po.js.map